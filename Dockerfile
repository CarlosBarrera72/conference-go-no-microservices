FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /app
COPY accounts accounts
COPY common common
COPY conference_go conference_go
COPY events events
COPY attendees attendees
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py
RUN pip install -r requirements.txt
RUN pip install requests
CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
