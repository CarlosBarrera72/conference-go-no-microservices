import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import State
from .acls import get_photo, get_weather_data

from .models import Conference, Location

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    # def get_picture(self, o):
    #     pic_data = get_photo(o.city, o.state.abbreviation)
    #     return {"picture_url" : pic_data.get('picture_url')}

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

@require_http_methods(["GET","POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content['location'])
            content['location'] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message" : "Invalid location id"},
                status = 400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe = False
        )


def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)

    # Use the city and state abbreviation of the Conference's Location
    city = conference.location.city
    state= conference.location.state.name
    # to call the get_weather_data ACL function and get back a dictionary
    weather_data = get_weather_data(city, state)
    # that contains the weather data

    response = {
        'weather':weather_data,
        'conference' : conference
        }

    # Include the weather data in the JsonResponse (see it in the dictionary?)

    return JsonResponse(
        response,
        encoder=ConferenceDetailEncoder, safe=False
    )

@require_http_methods(["GET","POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            locations,
            encoder=LocationDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content['state'])
            content['state']=state
        except State.DoesNotExist:
            return JsonResponse({"message" : "Invalid State abbreviation"}, status=400)

        # Use the city and state's abbreviation in the content dictionary
        # to call the get_photo ACL function
        photo_data =get_photo(content['city'])
        content['picture_url'] = photo_data
        # Use the returned dictionary to update the content dictionary
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method =="GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count,_ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
                return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
