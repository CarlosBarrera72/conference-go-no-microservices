from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(query):
    # # Use the Pexels API
    # pexels_key = PEXELS_API_KEY
    # # Create a dictionary for the headers to use in the request
    # headers = {"Authorization" : f'Bearer {pexels_key}'}
    # # Create the URL for the request with the city and state
    # query = f'{city} {state} city'
    # url = f'https://api.pexels.com/v1/search?query={query}'
    # # Make the request
    # response = requests.get(url, headers = headers)
    # # Parse the JSON response

    # print(f"Pexels API Response: {response.status_code}")
    # print(f"Pexels API Response Content: {response.text}")

    # payload = response.json()
    # # Return a dictionary that contains a `picture_url` key and
    # if 'photos' in payload.get('photos',[]):
    #     picture_url = payload["photos"][0]['src']['original']
    # else :
    #     picture_url = None

    # return {'picture_url' : picture_url}

    url = f'https://api.pexels.com/v1/search?query={query}'

    headers = {
        "Authorization" : PEXELS_API_KEY
    }

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']


def get_weather_data(city, state):

    geocoding_url =f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"

    geo_info = requests.get(geocoding_url)
    payload = geo_info.json()

    lat = payload[0].get('lat')
    lon = payload[0].get('lon')


    current_weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(current_weather_url)
    weather_payload = response.json()

    weather_description = weather_payload['weather'][0]['description']
    temperature = weather_payload['main'].get('temp')

    return {
        'description' : weather_description,
        'tempature' : temperature
    }
